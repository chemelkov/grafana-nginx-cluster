NGINX CLUSTER GRAFANA DOCKER COMPOSE

Nginx reverse proxy implements a health-check endpoint.
The endpoint `health-check` returns a json file with the contents.
All other requests are reverse proxied to the Grafana instance running inside the container

#docker network create one
#docker network create two
#docker-compose build 
#docker-compose up -d

To verify if it is working, go to:

- http://host:8001/health-check
- http://host:8002/health-check
- http://host:8001 Grafana main page
- http://host:8002 Grafana main page
